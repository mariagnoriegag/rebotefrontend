import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
const state = {
  count: 0,
  totalcost: 0,
  search: '',
  image: null,
  name: '',
  description: '',
  price: null,
  products: [],
  items: {
    itemsninos: [],
    itemsbebes: [],
    itemssnacks: [],
    itemsrebotegames: [],
    itemsferia: [],
    itemsotrosservicios: []
  },
  promociones: [],
  rebotevideo: 'rebote/rebotevideo.mp4'
}
const mutations = {
  increment (state) {
    state.count++
  },
  decrement (state) {
    if (state.count > 0) {
      state.count--
    }
  },
  loadListData (state, payload) {
    state.name = payload.name
    state.image = payload.image
    state.dimensions = payload.dimensions
    state.price = payload.price
  },
  addProduct (state, product) {
    state.products.push(product)
    state.count++
  },
  deleteProduct (state, product) {
    const index = state.products.indexOf(product)
    const indexninos = state.items.itemsninos.findIndex(x => x.name === product.name)
    const indexbebes = state.items.itemsbebes.findIndex(x => x.name === product.name)
    const indexsnacks = state.items.itemssnacks.findIndex(x => x.name === product.name)
    const indexrebotegames = state.items.itemsrebotegames.findIndex(x => x.name === product.name)
    const indexferia = state.items.itemsferia.findIndex(x => x.name === product.name)
    const indexotrosservicios = state.items.itemsotrosservicios.findIndex(x => x.name === product.name)
    confirm('¿Estás totalmente seguro de querer eliminar ' + state.products[index].name + '?') && state.products.splice(index, 1)
    if (indexninos !== -1) { state.items.itemsninos[indexninos].click = false }
    if (indexbebes !== -1) { state.items.itemsbebes[indexbebes].click = false }
    if (indexsnacks !== -1) { state.items.itemssnacks[indexsnacks].click = false }
    if (indexrebotegames !== -1) { state.items.itemsrebotegames[indexrebotegames].click = false }
    if (indexferia !== -1) { state.items.itemsferia[indexferia].click = false }
    if (indexotrosservicios !== -1) { state.items.itemsotrosservicios[indexotrosservicios].click = false }
    mutations.decrement(state)
  },
  removeProduct (state, product) {
    const index = state.products.indexOf(product)
    state.products.splice(index, 1)
    mutations.decrement(state)
  },
  removeallClickProducts (state) {
    for (let i = 0; i < state.items.itemsninos.length; i++) {
      state.items.itemsninos[i].click = false
    }
    for (let i = 0; i < state.items.itemsbebes.length; i++) {
      state.items.itemsbebes[i].click = false
    }
    for (let i = 0; i < state.items.itemssnacks.length; i++) {
      state.items.itemssnacks[i].click = false
    }
    for (let i = 0; i < state.items.itemsrebotegames.length; i++) {
      state.items.itemsrebotegames[i].click = false
    }
    for (let i = 0; i < state.items.itemsferia.length; i++) {
      state.items.itemsferia[i].click = false
    }
    for (let i = 0; i < state.items.itemsotrosservicios.length; i++) {
      state.items.itemsotrosservicios[i].click = false
    }
    state.count = 0
    state.products = []
  },
  updateItems (state, items) {
    if (state.items.itemsninos.length === 0) {
      let i = 0
      for (i in items.itemsninos) {
        state.items.itemsninos.push({...items.itemsninos[i], click: false, dialog: false})
      }
    } else {
      let i = 0
      for (i in items.itemsninos) {
        if (state.items.itemsninos[i].click === true) {
          state.items.itemsninos[i].click = true
        } else {
          state.items.itemsninos[i].click = false
        }
      }
    }
    if (state.items.itemsbebes.length === 0) {
      let i = 0
      for (i in items.itemsbebes) {
        state.items.itemsbebes.push({...items.itemsbebes[i], click: false, dialog: false})
      }
    } else {
      let i = 0
      for (i in items.itemsbebes) {
        if (state.items.itemsbebes[i].click === true) {
          state.items.itemsbebes[i].click = true
        } else {
          state.items.itemsbebes[i].click = false
        }
      }
    }
    if (state.items.itemssnacks.length === 0) {
      let i = 0
      for (i in items.itemssnacks) {
        state.items.itemssnacks.push({...items.itemssnacks[i], click: false, dialog: false})
      }
    } else {
      let i = 0
      for (i in items.itemssnacks) {
        if (state.items.itemssnacks[i].click === true) {
          state.items.itemssnacks[i].click = true
        } else {
          state.items.itemssnacks[i].click = false
        }
      }
    }
    if (state.items.itemsrebotegames.length === 0) {
      let i = 0
      for (i in items.itemsrebotegames) {
        state.items.itemsrebotegames.push({...items.itemsrebotegames[i], click: false, dialog: false})
      }
    } else {
      let i = 0
      for (i in items.itemsrebotegames) {
        if (state.items.itemsrebotegames[i].click === true) {
          state.items.itemsrebotegames[i].click = true
        } else {
          state.items.itemsrebotegames[i].click = false
        }
      }
    }
    if (state.items.itemsferia.length === 0) {
      let i = 0
      for (i in items.itemsferia) {
        state.items.itemsferia.push({...items.itemsferia[i], click: false, dialog: false})
      }
    } else {
      let i = 0
      for (i in items.itemsferia) {
        if (state.items.itemsferia[i].click === true) {
          state.items.itemsferia[i].click = true
        } else {
          state.items.itemsferia[i].click = false
        }
      }
    }
    if (state.items.itemsotrosservicios.length === 0) {
      let i = 0
      for (i in items.itemsotrosservicios) {
        state.items.itemsotrosservicios.push({...items.itemsotrosservicios[i], click: false, dialog: false})
      }
    } else {
      let i = 0
      for (i in items.itemsotrosservicios) {
        if (state.items.itemsotrosservicios[i].click === true) {
          state.items.itemsotrosservicios[i].click = true
        } else {
          state.items.itemsotrosservicios[i].click = false
        }
      }
    }
  },
  updatePromociones (state, promociones) {
    if (state.promociones.length === 0) {
      for (let i = 0; i < promociones.length; i++) {
        state.promociones.push({...promociones[i]})
      }
    } else {
      state.promociones = state.promociones
    }
  },
  updateVideo (state, rebotevideo) {
    state.rebotevideo = rebotevideo
  }
}
const actions = {
  increment: ({ commit }) => commit('increment'),
  decrement: ({ commit }) => commit('decrement'),
  loadListData: ({ commit }) => commit('loadListData'),
  addProduct: ({ commit }) => commit('addProduct'),
  deleteProduct: ({ commit }) => commit('deleteProduct'),
  removeProduct: ({ commit }) => commit('removeProduct'),
  removeallClickProducts: ({ commit }) => commit('removeallClickProducts'),
  updateItems: ({ commit }) => commit('updateItems'),
  updatePromociones: ({ commit }) => commit('updatePromociones')
}
export default new Vuex.Store({
  state,
  actions,
  mutations
})
