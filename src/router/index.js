import Vue from 'vue'
import Router from 'vue-router'
import VueCarousel from 'vue-carousel'
import ReboteHome from '@/components/ReboteHome'
import Reservar from '@/components/Reservar'
import Catalogo from '@/components/Catalogo'
import CatalogoNinos from '@/components/CatalogoNinos'
import CatalogoBebes from '@/components/CatalogoBebes'
import CatalogoSnacks from '@/components/CatalogoSnacks'
import CatalogoReboteGames from '@/components/CatalogoReboteGames'
import CatalogoFeria from '@/components/CatalogoFeria'
import CatalogoOtrosServicios from '@/components/CatalogoOtrosServicios'

Vue.use(Router)
Vue.use(VueCarousel)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'ReboteHome',
      component: ReboteHome
    },
    {
      path: '/reservar',
      name: 'Reservar',
      component: Reservar
    },
    {
      path: '/catalogo',
      name: 'Catalogo',
      component: Catalogo,
      children: [
        {
          path: '/',
          component: CatalogoNinos
        },
        {
          path: 'bebes',
          component: CatalogoBebes
        },
        {
          path: 'snacks',
          component: CatalogoSnacks
        },
        {
          path: 'rebotegames',
          component: CatalogoReboteGames
        },
        {
          path: 'feria',
          component: CatalogoFeria
        },
        {
          path: 'otrosservicios',
          component: CatalogoOtrosServicios
        }
      ]
    }
  ]
})
