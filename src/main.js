// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store/store'
import './firebase/firebase'

import {
  Vuetify,
  VApp,
  VBreadcrumbs,
  VNavigationDrawer,
  VFooter,
  VList,
  VBtn,
  VIcon,
  VGrid,
  VToolbar,
  VTooltip,
  VTabs,
  VCard,
  VBadge,
  VTextField,
  VPagination,
  VStepper,
  VForm,
  VCheckbox,
  VSelect,
  VDatePicker,
  VMenu,
  VDialog,
  VDataIterator,
  VDataTable,
  VDivider,
  VCarousel,
  transitions
} from 'vuetify'
import '../node_modules/vuetify/src/stylus/app.styl'

Vue.use(Vuetify, {
  components: {
    VApp,
    VBreadcrumbs,
    VNavigationDrawer,
    VFooter,
    VTooltip,
    VList,
    VBtn,
    VIcon,
    VGrid,
    VToolbar,
    VTabs,
    VCard,
    VBadge,
    VTextField,
    VPagination,
    VStepper,
    VForm,
    VCheckbox,
    VSelect,
    VDatePicker,
    VMenu,
    VDialog,
    VDataIterator,
    VDataTable,
    VDivider,
    VCarousel,
    transitions
  },
  theme: {
    primary: '#5E46F3',
    secondary: '#424242',
    accent: '#82B1FF',
    error: '#FF5252',
    info: '#2196F3',
    success: '#4CAF50',
    warning: '#FFC107'
  }
})

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
