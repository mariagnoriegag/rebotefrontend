import { initializeApp } from 'firebase'

const app = initializeApp({
  apiKey: 'AIzaSyBtrtYDRGrZz8taTSNf7ZSfUbQuklqmMTQ',
  authDomain: 'reboteana.firebaseapp.com',
  databaseURL: 'https://reboteana.firebaseio.com',
  projectId: 'reboteana',
  storageBucket: 'reboteana.appspot.com',
  messagingSenderId: '364213933694'
})

export const db = app.database()
export const storage = app.storage()
export const itemsRef = db.ref('items')
export const promocionesRef = db.ref('promociones')
